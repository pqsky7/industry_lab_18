package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.lang.management.BufferPoolMXBean;
import java.util.ArrayList;

/**
 * Created by qpen546 on 16/05/2017.
 */
public class StudentInfoGenerator {
    public ArrayList<String> firstNameList;
    public ArrayList<String> surnameList;
    public final int classSize = 550;
    public final int labNum = 3;
    public final int[] labSkillRequirements = {5, 15, 25, 65};
    public final int[] testSkillRequirements = {5, 20, 65, 90};
    public final int[] examSkillRequirements = {7, 20, 60, 90};
    public String output = "";

    private void start() {
        firstNameList = new ArrayList<>();
        surnameList = new ArrayList<>();
        readInput("FirstNames.txt", firstNameList);
        readInput("Surnames.txt", surnameList);
        for (int i = 1; i <= classSize; i++) {
            output += infoGenerator(i);
        }
        generateOutput(output);
    }

    public void readInput(String filePath, ArrayList list) {
        String line;
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String infoGenerator(int i) {
        String studentID = generateStudentID(i);
        String firstName = generateStudentName(firstNameList);
        String surname = generateStudentName(surnameList);
        String[] labMarks = new String[labNum];
        for (int j = 0; j < labNum; j++) {
            labMarks[j] = generateStudentMarks(labSkillRequirements, false);
        }
        String testMark = generateStudentMarks(testSkillRequirements, false);
        String examMark = generateStudentMarks(examSkillRequirements, true);
        return studentID + "\t" + surname + "\t" + firstName + "\t" + labMarks[0] + "\t" + labMarks[1] + "\t" + labMarks[2] + "\t" + testMark + "\t" + examMark + "\n";
    }

    public String generateStudentID(int i) {
        String studentsID = "";
        if (i < 10) {
            studentsID += "000" + i;
        } else if (i < 100) {
            studentsID += "00" + i;
        } else if (i < 1000) {
            studentsID += "0" + i;
        } else {
            studentsID += i;
        }
        return studentsID;
    }

    public String generateStudentName(ArrayList<String> nameList) {
        int randFNIndex = (int) (Math.random() * nameList.size());
        return nameList.get(randFNIndex);
    }

    public String generateStudentMarks(int[] skillRequirements, boolean failMethod) {
        int randStudentSkill = (int) (Math.random() * 101);
        String studentMark = "";
        if (randStudentSkill <= skillRequirements[0]) {
            if (failMethod) {
                studentMark += failMethod();
            } else {
                studentMark += (int) (Math.random() * 40); //[0,39]
            }
        } else if ((randStudentSkill > skillRequirements[0]) && (randStudentSkill <= skillRequirements[1])) {
            studentMark += ((int) (Math.random() * 10) + 40); //[40,49]
        } else if ((randStudentSkill > skillRequirements[1]) && (randStudentSkill <= skillRequirements[2])) {
            studentMark += ((int) (Math.random() * 20) + 50);//[50,69]
        } else if ((randStudentSkill > skillRequirements[2]) && (randStudentSkill <= skillRequirements[3])) {
            studentMark += ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            studentMark += ((int) (Math.random() * 11) + 90); //[90,100]
        }
        return studentMark;
    }

    public String failMethod(){
        String studentMark = "";
        int randDNSProb = (int) (Math.random() * 101);
        if (randDNSProb <= 5) {
            studentMark += ""; //DNS
        } else {
            studentMark += (int) (Math.random() * 40); //[0,39]
        }
        return studentMark;
    }

    public void generateOutput(String output) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("Data_Out.txt"))) {
            bw.write(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new StudentInfoGenerator().start();
    }
}